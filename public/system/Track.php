<?php

namespace system;
class Track
{
    private $class;
    private $method;
    private $params;

    public function __construct($class, $method, $params)
    {
        $this->class = $class;
        $this->method = $method;
        $this->params = $params;
    }

    public function __get($property)
    {
        return $this->$property;
    }

}