<?php

namespace system;

class Router
{
    public function getTrack(array $routes, string $uri): Track
    {
        foreach ($routes as $route) {
            $list1 = explode('/', $route->link);
            $list2 = explode('/', $uri);
            $params = [];
            $check = true;

            foreach ($list1 as $key => $item) {

                if (empty($item)) {
                    continue;
                }

                $skip = false;
                if (str_starts_with($item, ':')) {
                    $varName = ltrim($item, ':');
                    $params[$varName] = $list2[$key];
                    $skip = true;
                }

                if ($item !== $list2[$key] && !$skip) {
                    $check = false;
                    break;
                }
            }

            if ($check) {
                return new Track($route->class, $route->method, $params);
            }
        }

        return new Track('error', 'notFound', []);
    }


}
