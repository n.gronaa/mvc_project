<?php

namespace system;
class Route
{
    private $link;
    private $class;
    private $method;

    public function __construct($link, $class, $method)
    {
        $this->link = $link;
        $this->class = $class;
        $this->method = $method;
    }

    public function __get($property)
    {
        return $this->$property;
    }
}