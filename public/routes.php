<?php

use system\Route;

return [
    new Route('/product/:productId', 'catalog\\controller\\ProductController', 'getProduct')
];