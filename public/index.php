<?php

use system\Router;

require_once 'helper.php';
require_once 'config.php';
require_once 'Autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 'on');

$routes = require __DIR__ . '/routes.php';

$track = (new Router)->getTrack($routes, $_SERVER['REQUEST_URI']);

(new $track->class())->{$track->method}(...$track->params);




