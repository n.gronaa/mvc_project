<?php

function dump($data)
{
    print "<pre>";
    print_r($data);
    print "</pre>";
}
function dd($data)
{
    dump($data);
    die();
}