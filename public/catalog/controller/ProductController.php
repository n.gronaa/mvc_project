<?php
namespace catalog\controller;

use catalog\model\ProductModel;

class ProductController
{
    private $model;

    public function __construct()
    {
        $this->model = new ProductModel();
    }

    public function getProduct(int $productId)
    {
        $result = $this->model->getProductById($productId);

        echo json_encode($result);
    }
}