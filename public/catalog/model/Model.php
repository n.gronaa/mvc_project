<?php

namespace catalog\model;

class Model
{
    private $client;

    public function __construct()
    {
        $this->client = new \mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);

        if ($this->client->connect_error) {
            exit('Error');
        }
    }

    public function get(string $query): array
    {
        $result = $this->client->query($query);
        $rows = [];

        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    public function first(string $query): array
    {
        return $this->client->query($query)->fetch_assoc();
    }

    public function query(string $query)
    {
        return $this->client->query($query);

    }

    public function count(string $query): int
    {
        $result = $this->client->query($query);
        return $result->num_rows;
    }
}